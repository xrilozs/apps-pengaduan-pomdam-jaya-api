<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Send_mail {
    public function __construct() {}

    function send($email, $subject, $message, $attachment=null, $filename=null){
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
        // PHPMailer object
        $response = false;
        $mail = new PHPMailer();
      
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = SMTP_HOST; //sesuaikan sesuai nama domain hosting/server yang digunakan
        $mail->Username = SMTP_USER; // user email
        $mail->Password = SMTP_PASS; // password email
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;

        // $senderEmail = $isSender ? $email : COMPANY_NOREPLY_EMAIL;
        $mail->setFrom(COMPANY_NOREPLY_EMAIL, ''); // user email

        // Add a recipient
        $mail->addAddress($email); //email tujuan pengiriman email

        // Email subject
        $mail->Subject = $subject; //subject email

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = $message; // isi email
        $mail->Body = $mailContent;
        if($attachment && $filename) $mail->addStringAttachment($attachment, $filename);

        // Send email
        if(!$mail->send()){
            return 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            return 'Message has been sent';
        }
    }
}
?>