<?php
  function check_parameter($params){
    foreach($params as $param){
      if(is_null($param)){
        return false;
      }
    }
    return true;
  }

  function check_parameter_by_keys($data, $keys){
    foreach($keys as $key){
      if(!array_key_exists($key, $data)){
        return false;
      }
    }
    return true;
  }

  function get_uniq_id(){
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
      mt_rand( 0, 0xffff ),
      mt_rand( 0, 0x0C2f ) | 0x4000,
      mt_rand( 0, 0x3fff ) | 0x8000,
      mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
    );
  }


  function set_output($resp){
    $CI =& get_instance();
    $CI->output
        ->set_header('Access-Control-Allow-Origin: *')
        ->set_status_header($resp['code'])
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($resp));
  }

  function logging($type, $message, $data=null){
    log_message($type, $message);
    if($data){
      log_message($type, 'Request/Response: '.json_encode($data));
    }
  }

  function format_date_id($tanggal){
    // set locale untuk bahasa Indonesia
    setlocale(LC_TIME, 'id_ID');

    // konversi format tanggal ke format Indonesia
    $tanggal_indonesia = strftime('%d %B %Y', strtotime($tanggal));

    // konversi hari dalam bahasa Indonesia
    $hari = date('l', strtotime($tanggal));
    $daftar_hari = array(
        'Sunday' => 'Minggu',
        'Monday' => 'Senin',
        'Tuesday' => 'Selasa',
        'Wednesday' => 'Rabu',
        'Thursday' => 'Kamis',
        'Friday' => 'Jumat',
        'Saturday' => 'Sabtu'
    );
    $hari_indonesia = $daftar_hari[$hari];

    // daftar bulan dalam bahasa Indonesia
    $daftar_bulan = array(
        'January' => 'Januari',
        'February' => 'Februari',
        'March' => 'Maret',
        'April' => 'April',
        'May' => 'Mei',
        'June' => 'Juni',
        'July' => 'Juli',
        'August' => 'Agustus',
        'September' => 'September',
        'October' => 'Oktober',
        'November' => 'November',
        'December' => 'Desember'
    );

    // konversi nama bulan ke dalam bahasa Indonesia
    $tanggal_indonesia = strtr($tanggal_indonesia, $daftar_bulan);

    // tampilkan tanggal dan hari dalam format Indonesia
    echo $hari_indonesia . ', ' . $tanggal_indonesia;
  }
?>
