<?php
  require 'vendor/autoload.php';
  use Firebase\JWT\JWT;

  function verify_admin_token($header, $allowed_role=null){
    $CI =& get_instance();
    $resp_obj = new Response_api();
    
    #check header
    if(isset($header['Authorization'])){
      list(, $token) = explode(' ', $header['Authorization']);
    }else{
      $resp_obj->set_response(401, "failed", message("unauthorized access"));
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check token
    try {
      $jwt = JWT::decode($token, ACCESS_TOKEN_SECRET, ['HS256']);
    } catch (Exception $e) {
      $resp_obj->set_response(401, "failed", message("invalid token"));
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    $email = $jwt->email;
    $admin = $CI->admin_model->get_admin_by_email($email);
    
    #check user admin exist
    if(is_null($admin)){
      $resp_obj->set_response(401, "failed", action_result("admin", "not found"), $email);
      $resp = $resp_obj->get_response();
      return $resp;
    }else{
      if(!$admin->is_active){
        $resp_obj->set_response(401, "failed", action_result("admin", "not found"));
        $resp = $resp_obj->get_response();
        return $resp;
      }
    }
    
    #check allowed role
    if($allowed_role){
      if(!in_array($admin->role, $allowed_role)){
        $resp_obj->set_response(401, "failed", message("unauthorized access"));
        $resp = $resp_obj->get_response();
        return $resp;
      }
    }
    
    $data = array('admin'=>$admin);
    $resp_obj->set_response(200, "success", action_result("authectication", "success"), $data);
    $resp = $resp_obj->get_response();
    return $resp;
  }

  function verify_user_token($header){
    $CI =& get_instance();
    $resp_obj = new Response_api();
    
    #check header
    if(isset($header['token'])){
      $token = $header['token'];
    }else{
      $resp_obj->set_response(401, "failed", message("unauthorized access"));
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check token
    $user_token = $CI->token_model->get_token_by_token($token);
    if(is_null($user_token)){
      $resp_obj->set_response(401, "failed", message("invalid token"));
      $resp = $resp_obj->get_response();
      return $resp;
    }

    #check user
    $user = $CI->user_model->get_user_by_id($user_token->user_id);
    if(is_null($user)){
      $resp_obj->set_response(401, "failed", action_result("user", "not found"));
      $resp = $resp_obj->get_response();
      return $resp;
    }

    if($user->status != 'ACTIVE'){
      #expire token
      $CI->token_model->expire_token($user_token->id);
      $resp_obj->set_response(401, "failed", message("unauthorized access"));
      $resp = $resp_obj->get_response();
      return $resp;
    }

    $data = array('user'=>$user, 'token'=>$user_token);

    $resp_obj->set_response(200, "success", action_result("authectication", "success"), $data);
    $resp = $resp_obj->get_response();
    return $resp;
  }

?>