<?php
  function report_respond_push_notification($user, $message){
    $data = array(
      "title" => message('report responded'),
      "message" => $message
    );

    $fields = array(
      "to"                => $user->firebase_token,
      "data"              => $data,
      // "time_to_live"      => 30,
      // "delay_while_idle"  => true
    );

    $headers = array(
      "Authorization: key=".PUSHNOTIF_KEY,
      "Content-Type: application/json"
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, PUSHNOTIF_URL);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
  }
?>