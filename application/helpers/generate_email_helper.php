<?php
    function send_report_email($email, $report){
        $CI =& get_instance();

        $data['report'] = $report;
        $html = $CI->load->view('template/report_email', $data, true);
        $mail = new Send_mail();
        $email_resp = $mail->send($email, 'Notifikasi Laporan Baru - Apps Pengaduan Pomdam Jaya', $html);
        return $email_resp;
    }
?>