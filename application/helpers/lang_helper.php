<?php
  function message($key){
    $CI =& get_instance();
    return $CI->lang->line($key);
  }

  function action_result($action, $result){
    $CI =& get_instance();
    return $CI->lang->line($action) . " - " . $CI->lang->line($result);
  }
?>