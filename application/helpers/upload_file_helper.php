<?php
  function upload_image($file, $destination, $is_create_thumbnail=true) {
    $ok_ext = array(
      'jpg',
      'png',
      'jpeg',
      'bmp'
    );

    $filename       = explode(".", $file["name"]);
    $file_extension = $filename[count($filename) - 1];
    $file_weight    = $file['size'];
    $file_type      = $file['type'];
    if($file['error']){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload image failed!'
      ); 
    }
    if (!in_array(strtolower($file_extension), $ok_ext)) {
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'File with this type is not allowed!'
      ); 
    }
    if($file_weight > 12097152){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Max file size is 10MB!'
      );
    }
    $file_new_name = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
    if (!move_uploaded_file($file['tmp_name'], $destination . $file_new_name)) {
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload image failed!'
      );
    }
    $origin_img = $destination . $file_new_name;

    $data = array(
      'status' => 'success',
      'data' => array(
        "url" => $origin_img
      )
    );
    return $data;
  }

  function upload_document($file, $destination) {
    $ok_ext = array(
      'doc',
      'docx',
      'pdf',
      'txt'
    );

    $filename       = explode(".", $file["name"]);
    $file_extension = $filename[count($filename) - 1];
    $file_weight    = $file['size'];
    $file_type      = $file['type'];
    if($file['error']){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload document failed!'
      ); 
    }
    if (!in_array(strtolower($file_extension), $ok_ext)) {
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'File with this type is not allowed!'
      ); 
    }
    if($file_weight > 20097152){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Max file size is 20MB!'
      );
    }
    $file_new_name = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
    if (!move_uploaded_file($file['tmp_name'], $destination . $file_new_name)) {
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload document failed!'
      );
    }

    $origin_doc = $destination . $file_new_name;
    $doc = array(
      'docUrl' => $origin_doc
    );

    $data = array(
      'status' => 'success',
      'data' => $doc
    );
    return $data;
  }

  function upload_video($file, $destination) {
    $ok_ext = array(
      'mov',
      'mp4',
      'mkv',
      'webm',
      '3gp'
    );

    $filename       = explode(".", $file["name"]);
    $file_extension = $filename[count($filename) - 1];
    $file_weight    = $file['size'];
    $file_type      = $file['type'];
    if($file['error']){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload video failed!'
      ); 
    }
    if (!in_array(strtolower($file_extension), $ok_ext)) {
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'File with this type is not allowed!'
      ); 
    }
    if($file_weight > 100097152){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Max file size is 100MB!'
      );
    }
    $file_new_name = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
    if (!move_uploaded_file($file['tmp_name'], $destination . $file_new_name)) {
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload video failed!'
      );
    }

    $origin_vid = $destination . $file_new_name;

    $data = array(
      'status' => 'success',
      'data' => array(
        "url" => $origin_vid
      )
    );
    return $data;
  }
?>