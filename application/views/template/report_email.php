<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <style>
    	  body{
    	    font-family: Helvetica, sans-serif;
    	    height: 100%;
            margin: 0px;
        }
        img.center {
            display: block;
            margin: 0 auto;
        }
        #container {
            width:100%; 
            height:100vh;
    	    background-color: #8ee4af;
        }
        .blank-side {
            width:15%
        }
        #body {
            width:70%;
            height: 100%;
            background-color:white;
            margin: 20px;
            padding:20px;
            border-radius: 10px;
        }
        #detail{
            text-align: center;
        }
        #header {
            text-align: center;
            margin-top: 20px;
            margin-bottom: 50px;
        }
        .logo {
            height: 80px;
        }
        #google-play{
            width: 100px;
        }
        #content {
          /* text-align: center; */
          margin: auto;
          width: 70%;
          font-size: 16px;
          text-align: justify;
        }
        #content-header {
          margin: auto;
          width: 50%;
          font-size: 16px;
          text-align: justify;
        }
        #footer {
            padding-top: 40px;
            text-align: center;
            font-family: "Open Sans", sans-serif;
            font-size: 12px;
        }
        #footer-title {
            font-weight: bold;
            color: green;
            font-size: 14px;
        }
    </style>
    </head>
    <body>
    <table id="container">
      <tr>
        <td class="blank-side"></td>
        <td>
            <table width="100%" height="90%">
      		    <tr><td></td></tr>
      		    <tr>
      		        <td id="body">
      		    	    <div>
                      <table id="content-header">
                        <tr>
                          <td>
                            <img src="<?=ASSETS;?>web/logo.png" class="center logo">
                          </td>
                          <td>
                            <img src="<?=ASSETS;?>web/logo2.png" class="center logo">
                          </td>
                        </tr>
                      </table>
                      <div id="header">
                          <h1>Notifikasi Laporan Baru</h1>
                      </div>
                      <div id="content">
                          <p>
                              Terdapat laporan baru di Aplikasi <b>Pengaduan Pomdam Jaya</b>.
                          </p>
                          <p>
                            <table>
                              <tr>
                                <td><b>Nama Pelapor</b></td>
                                <td>: <?=$report['user_fullname'];?></td>
                              </tr>
                              <tr>
                                <td><b>Pesan</b></td>
                                <td>: <?=$report['message'];?></td>
                              </tr>
                              <tr>
                                <td><b>Lokasi</b></td>
                                <td>: <?=$report['location'];?></td>
                              </tr>
                              <tr>
                                <td><b>Tanggal</b></td>
                                <td>: <?=format_date_id($report['date']);?></td>
                              </tr>
                              <tr>
                                <td><b>Waktu</b></td>
                                <td>: <?=$report['time'];?></td>
                              </tr>
                            </table>
                          </p>
                          <p align="justify">
                            Klik <a href="<?=CMS_URL;?>report/detail/<?=$report['id'];?>" target="_blank"><b>disini</b></a> untuk memproses laporan
                          </p>
                      </div>
                      <div id="footer">
                          &copy; Copyright <span id="footer-title"><?=COMPANY_NAME;?></span>. All Rights Reserved
                      </div>
			            </div>
      		        </td>
      		    </tr>
      		    <tr><td></td></tr>
		    </table>
        </td>
        <td class="blank-side"></td>
      </tr>
    </table>
    </body>
</html>