<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <style>
    	body{
    	    font-family: Helvetica, sans-serif;
    	    height: 100%;
            margin: 0px;
        }
        img.center {
            display: block;
            margin: 0 auto;
        }
        #container {
            width:100%; 
            height:100vh;
    	    background-color: #8ee4af;
        }
        .blank-side {
            width:15%
        }
        #body {
            width:70%;
            background-color:white;
            margin: 20px;
            padding:20px;
            border-radius: 10px;
        }
        #detail{
            text-align: center;
        }
        #header {
            text-align: center;
            margin-top: 20px;
            margin-bottom: 50px;
        }
        #logo {
            width: 100px;
        }
        #google-play{
            width: 100px;
        }
        #content {
            text-align: center;
            font-size: 16px;
        }
        #footer {
            padding-top: 40px;
            text-align: center;
            font-family: "Open Sans", sans-serif;
            font-size: 12px;
        }
        #footer-title {
            font-weight: bold;
            color: #d79922;
            font-size: 14px;
        }
    </style>
    </head>
    <body>
    <table id="container">
      <tr>
        <td class="blank-side"></td>
        <td>
            <table width="100%">
      		    <tr><td></td></tr>
      		    <tr>
      		        <td id="body">
      		    	    <div>
                            <img src="<?=ASSETS;?>web/logo.png" class="center" id="logo">
                            <div id="header">
                                <h1>SELAMAT DATANG</h1>
                            </div>
                            <div id="content">
                                <p>
                                    Terima kasih telah mendaftar ke Aplikasi <b>Pengaduan Pomdam Jaya</b>. Klik link berikut untuk melakukan verifikasi:<br>
                                    $url 
                                </p>
                            </div>
                            <div id="footer">
                                &copy; Copyright <span id="footer-title"><?=COMPANY_NAME;?></span>. All Rights Reserved
                            </div>
			            </div>
      		        </td>
      		    </tr>
      		    <tr><td></td></tr>
		    </table>
        </td>
        <td class="blank-side"></td>
      </tr>
    </table>
    </body>
</html>