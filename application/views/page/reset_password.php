<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>APPS PENGADUAN POMDAM JAYA</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.5.0-2/css/all.min.css" rel="stylesheet">

    <style>
    	body{
    	    font-family: Helvetica, sans-serif;
            margin: 0px;
            height:100vh;
    	    background-color: #8ee4af;
        }
        .container .row{
            height:90vh;
        }
        .form-group{
            margin:10px;
        }
        #logo {
            width: 100px;
        }
        #footer {
            font-family: "Open Sans", sans-serif;
            font-size: 18px;
        }
        #footer-title {
            font-weight: bold;
            color: green;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center align-items-center">
                <div class="card w-50">
                    <div class="card-body">
                        <h5 class="card-title text-center font-weight-bold mb-4">
                            Ganti Kata Sandi
                        </h5>
                        <div id="alert">
                            <div class="alert alert-success" style="display:none;">
                                Kata sandi berhasil dirubah
                            </div>
                            <div class="alert alert-danger" style="display:none;">
                            </div>
                        </div>
      		    	    <form id="reset-password-form">
                            <div class="form-group">
                                <label>Kata sandi baru</label>
                                <input type="password" class="form-control" id="new_password" minlength="4" required>
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi sandi baru</label>
                                <input type="password" class="form-control" id="confirm_password" minlength="4" required>
                            </div>
                            <div class="form-group d-flex justify-content-end">
                                <button class="btn btn-success">Ganti Kata Sandi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <p id="footer" align="center" class="text-white">
            &copy; Copyright <span id="footer-title"><?=COMPANY_NAME;?></span>. All Rights Reserved
        </p>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script>
        let code = window.location.pathname.split("/").pop()

        $(document).ready(function(){
            $('#reset-password-form').submit(function(e){
                e.preventDefault()

                let new_password        = $('#new_password').val()
                let confirm_password    = $('#confirm_password').val()

                if(new_password != confirm_password){
                    $('.alert-danger').show()
                    $('.alert-danger').html('Kata sandi tidak sama')
                }else{
                    $.ajax({
                        async: true,
                        url: '<?=BASE_URL;?>user/set-new-password',
                        type: 'PUT',
                        data: JSON.stringify({
                            forget_password_code: code,
                            new_password: new_password
                        }),
                        error: function(res) {
                            $('.alert-success').hide()
                            $('.alert-danger').show()
                            $('.alert-danger').html('Mengatur ulang sandi gagal!')
                        },
                        success: function(res) {
                            $('.alert-success').show()
                            $('.alert-danger').hide()
                            $('#reset-password-form').hide();
                        }
                    });
                }


            })
        })
    </script>
</body>
</html>