<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Config extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }
  
  #path: /config [GET]
  function get_config(){
    $resp = new Response_api();

    #get config
    $config = $this->config_model->get_config();
    if(is_null($config)){
        logging('error', '/config [GET] - '.action_result('config', 'not found'));
        $resp->set_response(404, "failed", action_result('config', 'not found'));
        set_output($resp->get_response());
        return;
    }

    $config_attachment = $this->config_attachment_model->get_config_attachment();
    $video = [];
    $photo = [];
    foreach ($config_attachment as $item) {
      if($item->type == 'PHOTO'){
        array_push($photo, $item);
      }else{
        array_push($video, $item);
      }
    }

    $config->photo = $photo;
    $config->video = $video;
    
    #response
    logging('debug', '/config [GET] - '.action_result('get config', 'success'), $config);
    $resp->set_response(200, "success", action_result('get config', 'success'), $config);
    set_output($resp->get_response());
    return;
  }

  #path: /config [PUT]
  function update_config(){
    $resp = new Response_api();
    $request = json_decode($this->input->raw_input_stream, true);
    $allowed_role = array('SUPERADMIN', 'ADMIN');

    #check token
    $header = $this->input->request_headers();
    $resp_token = verify_admin_token($header, $allowed_role);
    if($resp_token['status'] == 'failed'){
      logging('error', '/config/web [PUT] - '.$resp_token['message']);
      set_output($resp_token);
      return;
    }

    #check request params
    $keys = array('twitter', 'instagram', 'youtube', 'description', 'logo_1');
    if(!check_parameter_by_keys($request, $keys)){
        logging('error', '/config/web [PUT] - '.message('missing param'), $request);
        $resp->set_response(400, "failed", message('missing param'));
        set_output($resp->get_response());
        return;
    }

    if(array_key_exists('id', $request)){
      #check config
      $config = $this->config_model->get_config_by_id($request['id']);
      if(is_null($config)){
        logging('error', '/config [PUT] - '.action_result('config', 'not found'), $request);
        $resp->set_response(404, "failed", action_result('config', 'not found'));
        set_output($resp->get_response());
        return;
      }

      #update config
      $flag = $this->config_model->update_config($request);
    }else{
      $request['id'] = get_uniq_id();
      $flag = $this->config_model->create_config($request);
    }

    if(!$flag){
        logging('error', '/config [PUT] - '.action_result('config', 'not change'), $request);
        $resp->set_response(500, "failed", action_result('config', 'not change'));
        set_output($resp->get_response());
        return;
    }
    logging('debug', '/config [PUT] - '.action_result('update config', 'success'), $request);
    $resp->set_response(200, "success", action_result('update config', 'success'), $request);
    set_output($resp->get_response());
    return;
  }

  #path: /config/upload-logo [POST]
  function upload_logo(){
    #init variable
    $resp_obj = new Response_api();

    #check token
    $header = $this->input->request_headers();
    $resp = verify_admin_token($header);
    if($resp['status'] == 'failed'){
        logging('error', '/config/upload-logo [POST] - '.$resp['message']);
        set_output($resp);
        return;
    }

    #check requested param
    $destination = "assets/img/";
    if (empty($_FILES['logo']['name'])) {
        logging('error', '/config/upload-logo [POST] - '.message('missing param'));
        $resp_obj->set_response(400, "failed", message('missing param'));
        set_output($resp_obj->get_response());
        return;
    }

    #upload image
    $file = $_FILES['logo'];
    $resp = upload_image($file, $destination);

    #response
    if($resp['status'] == 'failed'){
        logging('error', '/config/upload-logo [POST] - '.$resp['message']);
        $resp_obj->set_response(400, "failed", $resp['message']);
        set_output($resp_obj->get_response());
        return; 
    }
    $data = $resp['data'];

    $data['full_url'] = BASE_URL . $data['url'];
    logging('debug', '/config/upload-logo [POST] - '.message('upload attachment', 'success'), $data);
    $resp_obj->set_response(200, "success", message('upload attachment', 'success'), $data);
    set_output($resp_obj->get_response());
    return; 
  }
  
  #path: /config/upload-attachment [POST]
  function upload_attachment(){
    #init variable
    $resp_obj = new Response_api();

    #check token
    $header = $this->input->request_headers();
    $resp = verify_admin_token($header);
    if($resp['status'] == 'failed'){
        logging('error', '/config/upload-attachment [POST] - '.$resp['message']);
        set_output($resp);
        return;
    }
    
    $type = $this->input->post('type');
    $folder = $type == 'PHOTO' ? 'img' : 'video';

    #check requested param
    $destination = "assets/$folder/";
    if (empty($_FILES['file']['name'])) {
        logging('error', '/config/upload-attachment [POST] - '.message('missing param'));
        $resp_obj->set_response(400, "failed", message('missing param'), $_FILES);
        set_output($resp_obj->get_response());
        return;
    }

    #upload image
    $file = $_FILES['file'];
    if($type == 'PHOTO'){
      $resp = upload_image($file, $destination);
    }else{
      $resp = upload_video($file, $destination);
    }
    #response
    if($resp['status'] == 'failed'){
        logging('error', '/config/upload-attachment [POST] - '.$resp['message']);
        $resp_obj->set_response(400, "failed", $resp['message']);
        set_output($resp_obj->get_response());
        return; 
    }
    $data = $resp['data'];

    #save data
    $attachment = array(
      "id" => get_uniq_id(),
      "type" => $type,
      "url" => $data['url']
    );
    $flag = $this->config_attachment_model->create_config_attachment($attachment);
    if(!$flag){
      logging('error', '/config/upload-attachment [PUT] - '.action_result('config', 'not change'), $request);
      $resp->set_response(500, "failed", action_result('config', 'not change'));
      set_output($resp->get_response());
      return;
    }

    $data['full_url'] = BASE_URL . $data['url'];
    logging('debug', '/config/upload-attachment [POST] - '.message('upload attachment', 'success'), $data);
    $resp_obj->set_response(200, "success", message('upload attachment', 'success'), $data);
    set_output($resp_obj->get_response());
    return; 
  }

  #path: /config/remove-attachment [DELETE]
  function remove_attachment($id){
    #init variable
    $resp = new Response_api();
    $allowed_role = array('SUPERADMIN', 'ADMIN');

    #check token
    $header = $this->input->request_headers();
    $verify_resp = verify_admin_token($header, $allowed_role);
    if($verify_resp['status'] == 'failed'){
      logging('error', '/config/remove-attachment/'.$id.' [DELETE] - '.$verify_resp['message']);
      set_output($verify_resp);
      return;
    }

    #check config attachment
    $config_attachment = $this->config_attachment_model->get_config_attachment_by_id($id);
    if(is_null($config_attachment)){
      logging('error', '/config/remove-attachment/'.$id.' [DELETE] - '.action_result('attachment', 'not found'));
      $resp->set_response(404, "failed", action_result('attachment', 'not found'));
      set_output($resp->get_response());
      return;
    }

    #remove config attachment
    $flag = $this->config_attachment_model->remove_config_attachment($id);
    
    #response
    if(empty($flag)){
      logging('error', '/config/remove-attachment/'.$id.' [DELETE] - '.message('internal server error'));
      $resp->set_response(500, "failed", message('internal server error'));
      set_output($resp->get_response());
      return;
    }
    logging('debug', '/config/remove-attachment/'.$id.' [DELETE] - '.action_result('remove attachment', 'success'));
    $resp->set_response(200, "success", action_result('remove attachment', 'success'));
    set_output($resp->get_response());
    return;
  }
}

?>