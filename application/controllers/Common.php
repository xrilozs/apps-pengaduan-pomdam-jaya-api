<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Common extends CI_Controller {
  public function __construct($config = 'rest'){
    parent::__construct($config);
  }
  
  function index(){
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "Not Implemented.");
    $resp = $respObj->get_response();
    set_output($resp);
  }
  
  function test(){
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "APIs is working.");
    $resp = $respObj->get_response();
    set_output($resp);
  }
  
  function test_email(){
    $this->db->select("*,
      (SELECT u.fullname FROM user as u WHERE u.id = report.user_id) as user_fullname
    ");
    $query = $this->db->get('report');
    $report = $query->num_rows() > 0 ? $query->row() : null;
    if(is_null($report)){
      $respObj = new Response_api();
      $respObj->set_response(400, "failed", "No report data");
      $resp = $respObj->get_response();
      set_output($resp);
    }
    $emailResp = send_report_email('xrilozs@gmail.com', $report);
    
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "Send email...", $emailResp);
    $resp = $respObj->get_response();
    set_output($resp);
  }

  function test_email_template(){
    $this->db->select("*,
      (SELECT u.fullname FROM user as u WHERE u.id = report.user_id) as user_fullname
    ");
    $query = $this->db->get('report');
    $report = $query->num_rows() > 0 ? $query->row() : null;
    if(is_null($report)){
      $respObj = new Response_api();
      $respObj->set_response(400, "failed", "No report data");
      $resp = $respObj->get_response();
      set_output($resp);
    }

    $data['report'] = $report;
    $this->load->view('template/report_email', $data);
  }

  function generate_password($plain){
    $respObj = new Response_api();
    $hash = password_hash($plain, PASSWORD_DEFAULT);

    $respObj = new Response_api();
    $respObj->set_response(200, "success", "generate password success", $hash);
    $resp = $respObj->get_response();
    set_output($resp);
  }
  
}

?>