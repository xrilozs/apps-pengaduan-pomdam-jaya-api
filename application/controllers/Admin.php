<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Admin extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /admin/login [POST]
    function login(){
        #init req & resp
        $resp = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check request payload
        $keys = array('email', 'password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/admin/login [POST] - '.message('missing param'), $request);
            $resp->set_response(400, "failed", message('missing param'));
            set_output($resp->get_response());
            return;
        }

        #init variable
        $email = $request['email'];
        $password = $request['password'];

        #get user admin
        $admin = $this->admin_model->get_admin_by_email($email, 1);
        if(is_null($admin)){
            logging('error', '/admin/login [POST] - '.action_result('admin', 'not found'), $request);
            $resp->set_response(404, "failed", action_result('admin', 'not found'));
            set_output($resp->get_response());
            return;
        }

        #check user admin
        $verify = password_verify($password, $admin->password);
        if(!$verify){
            logging('error', '/admin/login [POST] - '.message('invalid password'), $request);
            $resp->set_response(400, "failed", message("invalid password"));
            set_output($resp->get_response());
            return;
        }

        #Create admin token      
        $token_expired = time() + (10 * 60);
        $payload = array(
          'email' => $admin->email,
          'exp' => $token_expired
        );
        #access token
        $access_token = JWT::encode($payload, ACCESS_TOKEN_SECRET);
        #refresh token
        $payload['exp'] = time() + (60 * 60);
        $refresh_token = JWT::encode($payload, REFRESH_TOKEN_SECRET);
      
        $response = array(
          'accessToken' => $access_token,
          'expiry' => date(DATE_ISO8601, $token_expired),
          'refreshToken' => $refresh_token,
          'role' => $admin->role,
          'fullname' => $admin->fullname
        );
        
        logging('debug', '/admin/login [POST] - '.action_result('login', 'success'), $response);
        $resp->set_response(200, "success", action_result('login', 'success'), $response);
        set_output($resp->get_response());
        return;
    }

    #path: admin/refresh [GET]
    function refresh(){
      $resp = new Response_api();

      #check header
      $header = $this->input->request_headers();
      if(isset($header['Authorization'])){
        list(, $token) = explode(' ', $header['Authorization']);
      }else{
        logging('debug', '/admin/refresh [GET] - Please use token to access this resource.');
        $resp->set_response(401, "failed", "Please use token to access this resource.");
        set_output($resp->get_response());
        return;
      }

      try {
        $jwt = JWT::decode($token, REFRESH_TOKEN_SECRET, ['HS256']);
      } catch (Exception $e) {
        logging('debug', '/admin/refresh [GET] - Invalid requested token');
        $resp->set_response(401, "failed", "Invalid requested token");
        set_output($resp->get_response());
        return;
      }
      $email = $jwt->email;
      $admin = $this->admin_model->get_admin_by_email($email, 1);

      #check user admin exist
      if(is_null($admin)){
        logging('debug', '/admin/refresh [GET] - User Admin not found');
        $resp->set_response(404, "failed", "User Admin not found");
        set_output($resp->get_response());
        return;
      }
      
      #generate new token
      $token_expired = time() + (10 * 60);
      $payload = array(
        'email' => $email,
        'exp' => $token_expired
      );
      #access token
      $access_token = JWT::encode($payload, ACCESS_TOKEN_SECRET);
      #refresh token
      $payload['exp'] = time() + (60 * 60);
      $refresh_token = JWT::encode($payload, REFRESH_TOKEN_SECRET);

      $response = array(
        'accessToken' => $access_token,
        'expiry' => date(DATE_ISO8601, $token_expired),
        'refreshToken' => $refresh_token,
        'role' => $admin->role,
        'fullname' => $admin->fullname
      );
      
      logging('debug', '/admin/refresh [GET] - Refresh User Admin success', $response);
      $resp->set_response(200, "success", "Refresh User Admin success", $response);
      set_output($resp->get_response());
      return;
    }

    #path: /admin/change_password [PUT]
    function change_password(){
        $resp = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        #check token
        $header = $this->input->request_headers();
        $verify_resp = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/admin/change_password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data']['admin'];
        
        #check request params
        $keys = array('old_password', 'new_password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/admin/change_password [PUT] - '.message('missing param'), $request);
            $resp->set_response(400, "failed", message('missing param'));
            set_output($resp->get_response());
            return;
        }
      
        if(!password_verify($request['old_password'], $admin->password)){
            logging('error', '/admin/change_password [PUT] - '.message('old password invalid'), $request);
            $resp->set_response(400, "failed", message("old password invalid"));
            set_output($resp->get_response());
            return;
        }

        #update admin
        $hash = password_hash($request['new_password'], PASSWORD_DEFAULT);
        $request['password'] = $hash;
        $request['id'] = $admin->id;
        $flag = $this->admin_model->change_password($admin->id, $hash);
        
        #response
        if(empty($flag)){
            logging('error', '/admin/change_password [PUT] - '.message('internal server error'), $request);
            $resp->set_response(500, "failed", message("internal server error"));
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/admin/change_password [PUT] - '.action_result('change password', 'success'), $request);
        $resp->set_response(200, "success", action_result("change password", "success"));
        set_output($resp->get_response());
        return;
    }

    #path: /admin [GET]
    function get_admin(){
        #init variable
        $resp = new Response_api();
        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $role = $this->input->get('role');
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);
        $allowed_role = array('SUPERADMIN');

        #check token
        $header = $this->input->request_headers();
        $verify_resp = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/admin [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data']['admin'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/admin [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get user admin
        $start = $page_number * $page_size;
        $order = array('field'=>'created_at', 'order'=>'DESC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $admin = $this->admin_model->get_admins($search, $role, $order, $limit);
        $output['data'] = $admin;
        $output['records_total'] = $this->admin_model->count_admins($search, $role);
        $output['records_filtered'] = $output['records_total'];
        
        #response
        if(empty($draw)){
          logging('debug', '/admin [GET] - Get user admin is success', $admin);
          $resp->set_response(200, "success", "Get user admin is success", $admin);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/admin [GET] - Get user admin is success', $output);
          $resp->set_response_datatable(200, $output['data'], $draw, $output['records_total'], $output['records_filtered']);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /admin/by_id/$id [GET]
    function get_admin_by_id($id){
        $resp = new Response_api();
        $allowed_role = array('SUPERADMIN');

        #check token
        $header = $this->input->request_headers();
        $verify_resp = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/admin/by_id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get user admin by id
        $admin = $this->admin_model->get_admin_by_id($id);
        if(is_null($admin)){
            logging('error', '/admin/by_id/'.$id.' [GET] - User admin not found');
            $resp->set_response(404, "failed", "User admin not found");
            set_output($resp->get_response());
            return;
        }
        unset($admin->password);

        #response
        logging('debug', '/admin/by_id/'.$id.' [GET] - Get user admin by id success', $admin);
        $resp->set_response(200, "success", "Get user admin by id success", $admin);
        set_output($resp->get_response());
        return;
    }

    #path: /admin/profile [GET]
    function get_profile(){
        $resp = new Response_api();
  
        #check token
        $header = $this->input->request_headers();
        $verify_resp = verify_admin_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/admin/profile [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data']['admin'];
        unset($admin->password);
  
        #response
        logging('debug', '/admin/profile [GET] - '.action_result('get profile', 'success'), $admin);
        $resp->set_response(200, "success", action_result('get profile', 'success'), $admin);
        set_output($resp->get_response());
        return;
    }

    #path: /admin [POST]
    function create_admin(){
        $resp = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        $allowed_role = array('SUPERADMIN');
        
        #check token
        $header = $this->input->request_headers();
        $verify_resp = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/admin [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $admin = $verify_resp['data']['admin'];
        
        #check request params
        $keys = array('fullname', 'email', 'password', 'role');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/admin [POST] - '.message("missing param"), $request);
            $resp->set_response(400, "failed", message("missing param"));
            set_output($resp->get_response());
            return;
        }

        #check duplicate email
        $admin = $this->admin_model->get_admin_by_email($request['email']);
        if($admin){
            logging('error', '/admin [POST] - '.message("email registered"), $request);
            $resp->set_response(400, "failed", message("email registered"));
            set_output($resp->get_response());
            return;
        }

        #init variable
        $request['id'] = get_uniq_id();

        #create user admin
        $hash = password_hash($request['password'], PASSWORD_DEFAULT);
        $request['password'] = $hash;
        $flag = $this->admin_model->create_admin($request);
        
        #response
        if(!$flag){
            logging('error', '/admin [POST] - '.message('internal server error'), $request);
            $resp->set_response(500, "failed", message("Internal server error"));
            set_output($resp->get_response());
            return;
        }

        unset($request['password']);
        logging('debug', '/admin [POST] - '.action_result('create admin', 'success'), $request);
        $resp->set_response(200, "success", action_result('create admin', 'success'), $request);
        set_output($resp->get_response());
        return;
    }

    #path: /admin [PUT]
    function update_admin(){
        $resp = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        $allowed_role = array('SUPERADMIN');

        #check token
        $header = $this->input->request_headers();
        $verify_resp = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/admin [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'fullname', 'email', 'role');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/admin [PUT] - '.message('missing param'), $request);
            $resp->set_response(400, "failed", message('missing param'));
            set_output($resp->get_response());
            return;
        }

        #check user admin
        $admin = $this->admin_model->get_admin_by_id($request['id']);
        if(is_null($admin)){
            logging('error', '/admin [PUT] - '.action_result('admin', 'not found'), $request);
            $resp->set_response(404, "failed", action_result("admin", "not found"));
            set_output($resp->get_response());
            return;
        }

        #check changes of email
        if($admin->email != $request['email']){
            #check duplicate email
            $admin_exist = $this->admin_model->get_admin_by_email($request['email']);
            if($admin_exist){
                logging('error', '/admin [POST] - '.message('email registered'), $request);
                $resp->set_response(400, "failed", message("email registered"));
                set_output($resp->get_response());
                return;
            }
        }

        #update admin
        $request['password'] = $admin->password;
        $request['is_active'] = $admin->is_active;
        $flag = $this->admin_model->update_admin($request);
        
        #response
        if(empty($flag)){
            logging('error', '/admin [PUT] - '.message('internal server error'), $request);
            $resp->set_response(500, "failed", message("internal server error"));
            set_output($resp->get_response());
            return;
        }

        unset($request['password']);
        logging('debug', '/admin [PUT] - '.action_result('update admin', 'success'), $request);
        $resp->set_response(200, "success", action_result("update admin", "success"), $request);
        set_output($resp->get_response());
        return;
    }

    // #path: /admin/active/$id [PUT]
    function active($id){
        #init variable
        $resp = new Response_api();
        $allowed_role = array('SUPERADMIN');

        #check token
        $header = $this->input->request_headers();
        $verify_resp = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/admin/active/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check admin
        $admin = $this->admin_model->get_admin_by_id($id);
        if(is_null($admin)){
            logging('error', '/admin/active/'.$id.' [PUT] - '.action_result('admin', 'not found'));
            $resp->set_response(404, "failed", action_result("admin", "not found"));
            set_output($resp->get_response());
            return;
        }

        #active admin
        $flag = $this->admin_model->active($id);
        
        #response
        if(empty($flag)){
            logging('error', '/admin/active/'.$id.' [PUT] - '.message('internal server error'));
            $resp->set_response(500, "failed", message("internal server error"));
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/admin/active/'.$id.' [PUT] - '.action_result('active admin', 'success'));
        $resp->set_response(200, "success", action_result("active admin", "success"));
        set_output($resp->get_response());
        return;
    }

    #path: /admin/inactive/$id [PUT]
    function inactive($id){
        #init variable
        $resp = new Response_api();
        $allowed_role = array('SUPERADMIN');

        #check token
        $header = $this->input->request_headers();
        $verify_resp = verify_admin_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/admin/inactive/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check admin
        $admin = $this->admin_model->get_admin_by_id($id);
        if(is_null($admin)){
            logging('error', '/admin/inactive/'.$id.' [PUT] - '.action_result('admin', 'not found'));
            $resp->set_response(404, "failed", action_result('admin', 'not found'));
            set_output($resp->get_response());
            return;
        }

        #inactive admin
        $flag = $this->admin_model->inactive($id);
        
        #response
        if(!$flag){
            logging('error', '/admin/inactive/'.$id.' [PUT] - '.message('internal server error'));
            $resp->set_response(500, "failed", message("internal server error"));
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/admin/inactive/'.$id.' [PUT] - '.action_result("inactive admin", "success"));
        $resp->set_response(200, "success", action_result("inactive admin", "success"));
        set_output($resp->get_response());
        return;
    }
}