<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Notification extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /notification/by-user [GET]
    function get_notification_by_user(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $is_read        = $this->input->get('is_read');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/notification/by-user [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];

        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/notification/by-user [GET] - ".message("missing param"), array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", message("missing param"));
            set_output($resp_obj->get_response());
            return;
        }

        #get faqs
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $notification   = $this->notification_model->get_notification($user->id, $is_read, $order, $limit);
        $total          = $this->notification_model->count_notification($user->id, $is_read);

        // $grouped_notification = [];
        // foreach ($notification as $key => $item) {
        //     $grouped_notification[$item->date][$key] = $item;
        // }
        // asort($grouped_notification, SORT_LOCALE_STRING);

        #response
        logging('debug', '/notification/by-user [GET] - '.action_result("get notification", "success"));
        $resp_obj->set_response_paginated(200, "success", action_result("get notification", "success"), $notification, $total);
        set_output($resp_obj->get_response_paginated());
        return;
    }

    #path: /notification/by-user/detail/$id [GET]
    function get_notification_by_user_and_id($notification_id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/notification/by-user/detail/'.$notification_id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];

        #get notification detail
        $notification = $this->notification_model->get_notification_by_user_id_and_id($user->id, $notification_id);
        if(is_null($notification)){
            logging('error', '/notification/by-user/detail/'.$notification_id.' [GET] - '.action_result('notification', 'not found'));
            $resp_obj->set_response(404, "failed", action_result('notification', 'not found'));
            set_output($resp_obj->get_response());
            return;
        }

        $report = $this->report_model->get_report_by_id($notification->report_id);
        if(is_null($report)){
            $notification->report_detail = null;
        }else{
            $report_attachment = $this->report_attachment_model->get_report_attachment_by_report_id($report->id);
            $video = [];
            $photo = [];
            foreach ($report_attachment as $item_file) {
                if($item_file->type == 'PHOTO'){
                    array_push($photo, $item_file);
                }else{
                    array_push($video, $item_file);
                }
            }
            $report->photo = $photo;
            $report->video = $video;

            $notification->report_detail = $report;
        }

        #read notification
        $this->notification_model->read_notification($notification_id);

        #response
        logging('debug', '/notification/by-user/detail/'.$notification_id.' [GET] - '.action_result('get notification', 'success'), $notification);
        $resp_obj->set_response(200, "success", action_result('get notification', 'success'), $notification);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /notification/by-user/count [GET]
    function count_notification_by_user(){
        $resp_obj       = new Response_api();
        $is_read    = $this->input->get('is_read');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/notification/by-user/count [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];

        #get notification detail
        $total  = $this->notification_model->count_notification($user->id, $is_read);

        #response
        logging('debug', '/notification/by-user/count [GET] - '.action_result('get notification', 'success'), $total);
        $resp_obj->set_response(200, "success", action_result('get notification', 'success'), $total);
        set_output($resp_obj->get_response());
        return;
    }
}