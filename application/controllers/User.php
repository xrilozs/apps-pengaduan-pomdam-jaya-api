<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class User extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /user/login [POST]
    function login(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check request params
        $keys = array('email', 'password', 'firebase_token');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/user/login [POST] - '.message("missing param"), $request);
            $resp_obj->set_response(400, "failed", message("missing param"));
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $email          = $request['email'];
        $password       = $request['password'];
        $firebase_token = $request['firebase_token'];

        #get user
        $user = $this->user_model->get_user_by_email($email, 1);
        if(is_null($user)){
            logging('error', '/user/login [POST] - '.action_result("user", "not found"), $request);
            $resp_obj->set_response(404, "failed", action_result("user", "not found"));
            set_output($resp_obj->get_response());
            return;
        }

        #check password
        $verify = password_verify($password, $user->password);
        if(!$verify){
            logging('error', '/user/login [POST] - '.message('invalid password'), $request);
            $resp_obj->set_response(400, "failed", message('invalid password'));
            set_output($resp_obj->get_response());
            return;
        }

        #Create user token      
        $payload = array(
          'email' => $user->email
        );
        #access token
        $access_token = JWT::encode($payload, ACCESS_TOKEN_SECRET);

        $data_token = array(
            "id"        => get_uniq_id(),
            "user_id"   => $user->id,
            "token"     => $access_token
        );
        $flag = $this->token_model->create_token($data_token);
        if(!$flag){
            logging('error', '/user/login [POST] - '.action_result('create token', 'failed'), $data_token);
            $resp_obj->set_response(500, "failed", action_result('create token', 'failed'));
            set_output($resp_obj->get_response());
            return;
        }

        #update firebase token
        $this->user_model->change_firebase_token_user($user->id, $firebase_token);
      
        $response = array(
          'access_token'    => $access_token,
          'fullname'        => $user->fullname
        );
        
        logging('debug', '/user/login [POST] - '.action_result('login', 'success'), $response);
        $resp_obj->set_response(200, "success", action_result('login', 'success'), $response);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /user/logout [POST]
    function logout(){
        $resp_obj = new Response_api();
  
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/user/logout [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $token = $resp['data']['token'];

        $flag = $this->token_model->expire_token($token->id);
        if(!$flag){
            logging('error', '/user/logout [POST] - '.action_result("logout", "failed"));
            $resp_obj->set_response(400, "failed", action_result("logout", "failed"));
            $resp = $resp_obj->get_response();
            set_output($resp);
            return;
        }
  
        #response
        logging('debug', '/user/logout [GET] - '.action_result("logout", "success"));
        $resp_obj->set_response(200, "success", action_result("logout", "success"));
        $resp = $resp_obj->get_response();
        set_output($resp);
        return;
    }

    #path: /user/registration [POST]
    function registration(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
      
        #check request params
        $keys = array('fullname', 'handphone', 'email', 'password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/user/registration [POST] - '.message("missing param"), $request);
            $resp_obj->set_response(400, "failed", message("missing param"));
            set_output($resp_obj->get_response());
            return;
        }
      
        #check email existing
        $exist_user = $this->user_model->get_user_by_email($request['email'], 1);
        if($exist_user){
            logging('error', "/user/registration [POST] - ".message("email registered"), $request);
            $resp_obj->set_response(400, "failed", message("email registered"));
            set_output($resp_obj->get_response());
            return;
        }

        #create user
        $hash = password_hash($request['password'], PASSWORD_DEFAULT);
        $request['password'] = $hash;
        $request['id'] = get_uniq_id();
        $flag = $this->user_model->create_user($request);
        if(!$flag){
            logging('error', '/user/registration [POST] - '.message("internal server error"), $request);
            $resp_obj->set_response(500, "failed", message("internal server error"), $request);
            set_output($resp_obj->get_response());
            return;
        }

        #generate verification email
        $curr_timestamp = strtotime(date('Y-m-d'));
        $exp_timestamp = strtotime('+ 2 days', $curr_timestamp);
        $expired_date = date('Y-m-d', $exp_timestamp);
        $grouped_string = $request['email'] . $request['password'] . date("Y-m-d H:i:s");
        $hash_code = hash('md5', $grouped_string);
        $url = BASE_URL . "verification/" . $hash_code;

        $verification = array(
            "id" => get_uniq_id(),
            "user_id" => $request['id'],
            "verification_code" => $hash_code
        );
        $flag = $this->verification_model->create_verification($verification);
        if(!$flag){
            logging('debug', '/user/registration [POST] - '.message("internal server error"));
            $resp_obj->set_response(500, "success",  message("internal server error"));
            set_output($resp_obj->get_response());
            return;
        }
        
        $response = send_registration_email($request['email'], $url);
      
        #response
        logging('debug', '/user/registration [POST] - '.action_result("registration", "success"), $response);
        $resp_obj->set_response(200, "success", action_result("registration", "success"), $response);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /verification [GET]
    function verification($code){
        $resp_obj = new Response_api();

        $verification = $this->verification_model->get_verification_by_code($code);
        if(!$verification){
            logging('error', '/verification [GET] - '.message("invalid verification"));
            $this->load->view("page/verification_failed");
            return;
        }
        
        #update verification
        $flag = $this->verification_model->use_verification($verification->id);
        if(!$flag){
            logging('error', '/verification [GET] - '.message("verification", "failed"));
            $this->load->view("page/verification_failed");
            return;
        }
        
        #active user
        $flag = $this->user_model->active_user($verification->user_id);
        if(!$flag){
            logging('error', '/verification [GET] - '.message("verification", "failed"));
            $this->load->view("page/verification_failed");
            return;
        }

        logging('debug', '/verification [GET] - '.message("verification", "success"));
        $this->load->view("page/verification_success");
        return;
    }

    #path: /user/profile [GET]
    function profile(){
        $resp_obj = new Response_api();
  
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/user/profile [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];
        unset($user->password);
  
        #response
        logging('debug', '/user/profile [GET] - '.action_result('get profile', 'success'), $user);
        $resp_obj->set_response(200, "success", action_result('get profile', 'success'), $user);
        $resp = $resp_obj->get_response();
        set_output($resp);
        return;
    }

    #path: /user/change-password [PUT]
    function change_password(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/user/change-password [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];
        
        #check request params
        $keys = array('old_password', 'new_password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/user/change-password [PUT] - '.message('missing param'), $request);
            $resp_obj->set_response(400, "failed", message('missing param'));
            set_output($resp_obj->get_response());
            return;
        }
      
        if(!password_verify($request['old_password'], $user->password)){
            logging('error', '/user/change-password [PUT] - '.message('invalid password'), $request);
            $resp_obj->set_response(400, "failed", message('invalid password'));
            set_output($resp_obj->get_response());
            return;
        }

        #update user
        $hash = password_hash($request['new_password'], PASSWORD_DEFAULT);
        $request['password'] = $hash;
        $request['id'] = $user->id;
        $flag = $this->user_model->change_password_user($user->id, $hash);
        if(!$flag){
            logging('error', '/user/change_password [PUT] - '.message("internal server error"), $request);
            $resp_obj->set_response(500, "failed", message("internal server error"));
            set_output($resp_obj->get_response());
            return;
        }

        logging('debug', '/user/change_password [PUT] - '.action_result("change password", "success"), $request);
        $resp_obj->set_response(200, "success", action_result("change password", "success"));
        set_output($resp_obj->get_response());
        return;
    }

    #path: /user/firebase-token [PUT]
    function change_firebase_token(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/user/firebase-token [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];
        
        #check request params
        $keys = array('firebase_token');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/user/firebase-token [PUT] - '.message('missing param'), $request);
            $resp_obj->set_response(400, "failed", message('missing param'));
            set_output($resp_obj->get_response());
            return;
        }

        #update firebase token
        $flag = $this->user_model->change_firebase_token_user($user->id, $request['firebase_token']);
        if(!$flag){
            logging('error', '/user/firebase-token [PUT] - '.message("internal server error"), $request);
            $resp_obj->set_response(500, "failed", message("internal server error"));
            set_output($resp_obj->get_response());
            return;
        }

        logging('debug', '/user/firebase-token [PUT] - '.action_result("change firebase token", "success"), $request);
        $resp_obj->set_response(200, "success", action_result("change firebase token", "success"));
        set_output($resp_obj->get_response());
        return;
    }

    #path: /user [PUT]
    function update_user(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/user [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];
        
        #check request params
        $keys = array('fullname', 'handphone');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/user [PUT] - '.message('missing param'), $request);
            $resp_obj->set_response(400, "failed", message('missing param'));
            set_output($resp_obj->get_response());
            return;
        }

        #update user
        $request['id']              = $user->id;
        $request['password']        = $user->password;
        $request['firebase_token']  = $user->firebase_token;
        $request['email']           = $user->email;
        $request['status']          = $user->status;
        $flag = $this->user_model->update_user($request);

        unset($request['password']);
        logging('debug', 'user [PUT] - '.action_result('update profile', 'success'), $request);
        $resp_obj->set_response(200, "success", action_result('update profile', 'success'), $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /user [GET]
    function get_user(){
        #init variable
        $resp_obj = new Response_api();
        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $status = $this->input->get('status');
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);
        // $allowed_role = array('SUPERADMIN');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/user [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/user [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get user
        $start = $page_number * $page_size;
        $order = array('field'=>'created_at', 'order'=>'DESC');
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $user = $this->user_model->get_user($search, $status, $order, $limit);
        $output['data'] = $user;
        $output['records_total'] = $this->user_model->count_user($search, $status);
        $output['records_filtered'] = $output['records_total'];
        
        #response
        if(empty($draw)){
          logging('debug', '/user [GET] - Get user is success', $output['data']);
          $resp_obj->set_response(200, "success", "Get user is success", $output['data']);
          set_output($resp_obj->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/user [GET] - Get user is success', $output);
          $resp_obj->set_response_datatable(200, $output['data'], $draw, $output['records_total'], $output['records_filtered']);
          set_output($resp_obj->get_response_datatable());
          return;
        } 
    }

    #path: /user/by_id/$id [GET]
    function get_user_by_id($id){
        $resp_obj = new Response_api();
        // $allowed_role = array('SUPERADMIN');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/user/by_id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get user detail
        $user = $this->user_model->get_user_by_id($id);
        if(is_null($user)){
            logging('error', '/user/by_id/'.$id.' [GET] - User not found');
            $resp_obj->set_response(404, "failed", "User not found");
            set_output($resp_obj->get_response());
            return;
        }
      
        #response
        logging('debug', '/user/by_id/'.$id.' [GET] - Get user by id success', $user);
        $resp_obj->set_response(200, "success", "Get user by id success", $user);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /user/inactive/$id [PUT]
    function inactive($id){
        #init variable
        $resp_obj = new Response_api();
        // $allowed_role = array('SUPERADMIN');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/user/block/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check user
        $user = $this->user_model->get_user_by_id($id);
        if(is_null($user)){
            logging('error', '/user/block/'.$id.' [PUT] - User not found');
            $resp_obj->set_response(404, "failed", "User not found");
            set_output($resp_obj->get_response());
            return;
        }

        #inactive user
        $flag = $this->user_model->inactive_user($id);
        
        #response
        if(!$flag){
            logging('error', '/user/block/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/user/inactive/'.$id.' [PUT] - block user success');
        $resp_obj->set_response(200, "success", "block user success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /user/active/$id [PUT]
    function active($id){
        #init variable
        $resp_obj = new Response_api();
        // $allowed_role = array('SUPERADMIN');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/user/active/'.$id.' [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check user
        $user = $this->user_model->get_user_by_id($id);
        if(is_null($user)){
            logging('error', '/user/active/'.$id.' [PUT] - User not found');
            $resp_obj->set_response(404, "failed", "User not found");
            set_output($resp_obj->get_response());
            return;
        }

        #active user
        $flag = $this->user_model->active_user($id);
        
        #response
        if(!$flag){
            logging('error', '/user/active/'.$id.' [PUT] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/user/active/'.$id.' [PUT] - active user success');
        $resp_obj->set_response(200, "success", "active user success");
        set_output($resp_obj->get_response());
        return;
    }
  
    #path: /user/count [GET]
    function get_total_user(){
      $resp_obj = new Response_api();
      $status = $this->input->get('status');

      #check token
      $header = $this->input->request_headers();
      $resp = verify_admin_token($header);
      if($resp['status'] == 'failed'){
          logging('error', '/user/count [GET] - '.$resp['message']);
          set_output($resp);
          return;
      }
      
      $count = $this->user_model->count_user(null, $status);

      #response
      logging('debug', '/user/count [GET] - '.action_result("get user", "success"), $count);
      $resp_obj->set_response(200, "success", action_result("get user", "success"), $count);
      set_output($resp_obj->get_response());
      return;
    }

    #path: /user/forget-password [POST]
    function create_forget_password(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check request params
        $keys = array('email');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/user/forget-password [POST] - '.message('missing param'), $request);
            $resp_obj->set_response(400, "failed", message('missing param'));
            set_output($resp_obj->get_response());
            return;
        }

        #get user
        $user = $this->user_model->get_user_by_email($request['email'], 1);
        if(is_null($user)){
            logging('error', '/user/forget-password [POST] - '.action_result('user', 'not found'), $request);
            $resp_obj->set_response(400, "failed", action_result('user', 'not found'));
            set_output($resp_obj->get_response());
            return;
        }

        #generate forget password email
        $grouped_string = $request['email'] . date("Y-m-d H:i:s");
        $hash_code      = hash('md5', $grouped_string);
        $url            = BASE_URL . "reset-password/" . $hash_code;

        $data_forget_password = array(
            "id"                    => get_uniq_id(),
            "user_id"               => $user->id,
            "forget_password_code"  => $hash_code
        );
        $flag = $this->forget_password_model->create_forget_password($data_forget_password);
        if(!$flag){
            logging('debug', '/user/forget-password [POST] - '.message('internal server error'));
            $resp_obj->set_response(500, "success", message('internal server error'));
            set_output($resp_obj->get_response());
            return;
        }

        $response = send_forget_password_email($request['email'], $url);
        
        logging('debug', '/user/forget-password [POST] - '.action_result('request forget password', 'success'), $response);
        $resp_obj->set_response(200, "success", action_result('request forget password', 'success'), $response);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /reset-password [GET]
    function reset_password($code){
        $resp_obj = new Response_api();

        $forget_password = $this->forget_password_model->get_forget_password_by_code($code);
        if(!$forget_password){
            logging('error', '/reset-password [GET] - '.message("invalid forget password"));
            $this->load->view("page/reset_password_failed");
            return;
        }

        $this->load->view("page/reset_password");
        return;
    }

    #path: /user/set-new-password [POST]
    function set_new_password(){
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check request params
        $keys = array('forget_password_code', 'new_password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/user/set-new-password [POST] - '.message('missing param'), $request);
            $resp_obj->set_response(400, "failed", message('missing param'));
            set_output($resp_obj->get_response());
            return;
        }

        #get request forget password
        $forget_password = $this->forget_password_model->get_forget_password_by_code($request['forget_password_code']);
        if(is_null($forget_password)){
            logging('error', '/user/set-new-password [POST] - '.action_result('request forget password', 'invalid'));
            $resp_obj->set_response(404, "failed", action_result('request forget password', 'invalid'));
            set_output($resp_obj->get_response());
            return;
        }
      
        #change password
        $hash = password_hash($request['new_password'], PASSWORD_DEFAULT);
        $flag = $this->user_model->change_password_user($forget_password->user_id, $hash);
        if(!$flag){
            logging('error', '/user/set-new-password [POST] - '.message('internal server error'));
            $resp_obj->set_response(500, "failed", message('internal server error'));
            set_output($resp_obj->get_response());
            return;
        }
      
        #use forget password
        $this->forget_password_model->use_forget_password($forget_password->id);
        logging('debug', '/user/set-new-password [POST] - '.action_result('reset password', 'success'));
        $resp_obj->set_response(200, "success", action_result('reset password', 'success'));
        set_output($resp_obj->get_response());
        return;
    }
}