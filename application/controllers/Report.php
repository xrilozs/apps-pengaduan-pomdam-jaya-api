<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Report extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /report [GET]
    function get_report(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $user_id        = $this->input->get('user_id');
        $status         = $this->input->get('status');
        $draw           = $this->input->get('draw');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/report [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/report [GET] - ".message("missing param"), array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", message("missing param"));
            set_output($resp_obj->get_response());
            return;
        } 

        #get report
        $start  = $page_number * $page_size;
        $order  = array('field'=>'created_at', 'order'=>'DESC');
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $report = $this->report_model->get_report($user_id, $status, $order, $limit);
        $total  = $this->report_model->count_report($user_id, $status);

        #response
        if(empty($draw)){
            logging('debug', '/report [GET] - '.action_result("get report",  "success"));
            $resp_obj->set_response_paginated(200, "success", action_result("get report",  "success"), $report, $total);
            set_output($resp_obj->get_response_paginated());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/report [GET] - '.action_result("get report",  "success"));
            $resp_obj->set_response_datatable(200, $report, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /report/by-id/$id [GET]
    function get_report_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/report/by-id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get report detail
        $report = $this->report_model->get_report_by_id($id);
        if(is_null($report)){
            logging('error', '/report/by-id/'.$id.' [GET] - '.action_result("report", "not found"));
            $resp_obj->set_response(404, "failed", action_result("report", "not found"));
            set_output($resp_obj->get_response());
            return;
        }

        #get notification
        $notification                   = $this->notification_model->get_notification_by_report_id($report->id);
        $report->notification_detail    = $notification;

        #get attachment
        $report_attachment = $this->report_attachment_model->get_report_attachment_by_report_id($report->id);
        $video = [];
        $photo = [];
        foreach ($report_attachment as $item) {
            if($item->type == 'PHOTO'){
                array_push($photo, $item);
            }else{
                array_push($video, $item);
            }
        }

        $report->photo = $photo;
        $report->video = $video;

        #response
        logging('debug', '/report/by-id/'.$id.' [GET] - '.action_result("get report", "success"), $report);
        $resp_obj->set_response(200, "success", action_result("get report", "success"), $report);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /report/count [GET]
    function count_report(){
        $resp_obj   = new Response_api();
        $status  = $this->input->get('status');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/report/count [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get report detail
        $total  = $this->report_model->count_report(null, $status);

        #response
        logging('debug', '/report/count [GET] - '.action_result('get report', 'success'), $total);
        $resp_obj->set_response(200, "success", action_result('get report', 'success'), $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /report/by-user [GET]
    function get_report_by_user(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $status         = $this->input->get('status');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/report/by-user [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];

        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/report/by-user [GET] - ".message("missing param"), array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", message("missing param"));
            set_output($resp_obj->get_response());
            return;
        }

        #get faqs
        $start  = $page_number * $page_size;
        $order  = array('field'=>'created_at', 'order'=>'DESC');
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $report = $this->report_model->get_report($user->id, $status, $order, $limit);
        $total  = $this->report_model->count_report($user->id, $status);

        foreach ($report as &$item) {
            $report_attachment = $this->report_attachment_model->get_report_attachment_by_report_id($item->id);
            $video = [];
            $photo = [];
            foreach ($report_attachment as $item_file) {
                if($item_file->type == 'PHOTO'){
                    array_push($photo, $item_file);
                }else{
                    array_push($video, $item_file);
                }
            }

            $item->photo = $photo;
            $item->video = $video;
        }

        #response
        logging('debug', '/report/by-user [GET] - '.action_result("get report", "success"));
        $resp_obj->set_response_paginated(200, "success", action_result("get report", "success"), $report, $total);
        set_output($resp_obj->get_response_paginated());
        return;
    }

    #path: /report/by-user/detail/$id [GET]
    function get_report_by_user_and_id($report_id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/report/by-user/detail/'.$report_id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];

        #get report detail
        $report = $this->report_model->get_report_by_user_id_and_id($user->id, $report_id);
        if(is_null($report)){
            logging('error', '/report/by-user/detail/'.$report_id.' [GET] - '.action_result('report', 'not found'));
            $resp_obj->set_response(404, "failed", action_result('report', 'not found'));
            set_output($resp_obj->get_response());
            return;
        }

        $report_attachment = $this->report_attachment_model->get_report_attachment_by_report_id($report_id);
        $video = [];
        $photo = [];
        foreach ($report_attachment as $item) {
            if($item->type == 'PHOTO'){
                array_push($photo, $item);
            }else{
                array_push($video, $item);
            }
        }

        $report->photo = $photo;
        $report->video = $video;

        #response
        logging('debug', '/report/by-user/detail/'.$report_id.' [GET] - '.action_result('get report', 'success'), $report);
        $resp_obj->set_response(200, "success", action_result('get report', 'success'), $report);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /report/by-user/count [GET]
    function count_report_by_user(){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/report/by-user/count [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];

        #get report detail
        $total  = $this->report_model->count_report($user->id);

        #response
        logging('debug', '/report/by-user/count [GET] - '.action_result('get report', 'success'), $total);
        $resp_obj->set_response(200, "success", action_result('get report', 'success'), $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /report [POST]
    function create_report(){
        #init req & res
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/report [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];
        
        #check request params
        $keys = array('message', 'title', 'date', 'time', 'longitude', 'latitude', 'location');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/report [POST] - '.message('missing param'), $request);
            $resp_obj->set_response(400, "failed", message('missing param'));
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id'] = get_uniq_id();
        $request['user_id'] = $user->id;
        $flag = $this->report_model->create_report($request);
        
        #response
        if(!$flag){
            logging('error', '/report [POST] - '.message('internal server error'), $request);
            $resp_obj->set_response(500, "failed", message('internal server error'));
            set_output($resp_obj->get_response());
            return;
        }

        #send email notification
        $request['user_fullname'] = $user->fullname;
        $resp = send_report_email(REPORT_EMAIL, $request);
        logging('debug', '/report [POST] - Email Report Response', $resp);

        logging('debug', '/report [POST] - '.action_result('create report', 'success'), $request);
        $resp_obj->set_response(200, "success", action_result('create report', 'success'), $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /report/respond [PUT]
    function respond_report(){
        #init req & res
        $resp_obj = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_admin_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/report/respond [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('report_id', 'message', 'title');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/report/respond [PUT] - '.message('missing param'), $request);
            $resp_obj->set_response(400, "failed", message('missing param'));
            set_output($resp_obj->get_response());
            return;
        }

        #check report
        $report = $this->report_model->get_report_by_id($request['report_id']);
        if(is_null($report)){
            logging('error', '/report/respond [PUT] - '.message('missing param'), $request);
            $resp_obj->set_response(400, "failed", action_result('report', 'not found'));
            set_output($resp_obj->get_response());
            return;
        }

        if($report->status != 'SENT'){
            logging('error', '/report/respond [PUT] - '.message('missing param'), $request);
            $resp_obj->set_response(400, "failed", message('report responded'));
            set_output($resp_obj->get_response());
            return;
        }
        
        #respond
        $flag = $this->report_model->respond_report($request['report_id']);
        if(!$flag){
            logging('error', '/report/respond [PUT] - '.action_result('respond report', 'failed'), $request);
            $resp_obj->set_response(500, "failed", action_result('respond report', 'failed'));
            set_output($resp_obj->get_response());
            return;
        }

        #create notification
        $request['id']      = get_uniq_id();
        $request['user_id'] = $report->user_id;
        $flag               = $this->notification_model->create_notification($request);
        if(!$flag){
            logging('error', '/report/respond [PUT] - '.message('internal server error'), $request);
            $resp_obj->set_response(500, "failed", message('internal server error'));
            set_output($resp_obj->get_response());
            return;
        }
        $user           = $this->user_model->get_user_by_id($report->user_id);
        $pushnotif_resp = report_respond_push_notification($user, $request['message']);
        logging('error', '/report/respond [PUT] - Pushnotif Response', $pushnotif_resp);

        logging('debug', '/report/respond [PUT] - '.action_result('respond report', 'success'), $request);
        $resp_obj->set_response(200, "success", action_result('respond report', 'success'), $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /report/upload-file [POST]
    function upload_file(){
        #init variable
        $resp_obj = new Response_api();
    
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/report/upload-file [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        $report_id  = $this->input->post('report_id');
        $type       = $this->input->post('type');
        $folder     = $type == 'PHOTO' ? 'img' : 'video';
    
        #check requested param
        $destination = "assets/$folder/";
        if (empty($_FILES['file']['name'])) {
            logging('error', '/report/upload-file [POST] - '.message('missing param'));
            $resp_obj->set_response(400, "failed", message('missing param'), $_FILES);
            set_output($resp_obj->get_response());
            return;
        }

        #check report
        $report = $this->report_model->get_report_by_id($report_id);
        if(is_null($report)){
            logging('error', '/report/upload-file [POST] - '.action_result('report', 'not found'));
            $resp_obj->set_response(404, "failed", action_result('report', 'not found'));
            set_output($resp_obj->get_response());
            return;
        }
    
        #upload file
        $file = $_FILES['file'];
        if($type == 'PHOTO'){
          $resp = upload_image($file, $destination);
        }else{
          $resp = upload_video($file, $destination);
        }
        #response
        if($resp['status'] == 'failed'){
            logging('error', '/report/upload-file [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data = $resp['data'];
    
        #save data
        $attachment = array(
          "id"          => get_uniq_id(),
          "report_id"   => $report_id,
          "type"        => $type,
          "url"         => $data['url']
        );
        $flag = $this->report_attachment_model->create_report_attachment($attachment);
        if(!$flag){
          logging('error', '/report/upload-file [POST] - '.action_result('upload attachment', 'failed'), $request);
          $resp->set_response(500, "failed", action_result('upload attachment', 'failed'));
          set_output($resp->get_response());
          return;
        }
    
        $data['full_url'] = BASE_URL . $data['url'];
        logging('debug', '/report/upload-file [POST] - '.action_result('upload attachment', 'success'), $data);
        $resp_obj->set_response(200, "success", action_result('upload attachment', 'success'), $data);
        set_output($resp_obj->get_response());
        return; 
    }
  
}