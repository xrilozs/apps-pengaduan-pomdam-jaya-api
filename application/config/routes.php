<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translateUri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'common';
$route['404_override'] = '';
$route['translateUri_dashes'] = FALSE;
$route['test'] = 'common/test';
$route['test_email'] = 'common/test_email';
$route['test-email-template'] = 'common/test_email_template';
$route['generate_password/(:any)']['GET'] = 'common/generate_password/$1';

/* API */
#Admin
$route['admin/login']['POST']           = 'admin/login';
$route['admin/refresh']['GET']          = 'admin/refresh';
$route['admin/change-password']['PUT']  = 'admin/change_password';
$route['admin']['GET']                  = 'admin/get_admin';
$route['admin/by-id/(:any)']['GET']     = 'admin/get_admin_by_id/$1';
$route['admin/profile']['GET']          = 'admin/get_profile';
$route['admin']['POST']                 = 'admin/create_admin';
$route['admin']['PUT']                  = 'admin/update_admin';
$route['admin/inactive/(:any)']['PUT']  = 'admin/inactive/$1';
$route['admin/active/(:any)']['PUT']    = 'admin/active/$1';

#Config
$route['config']['GET']                             = 'config/get_config';
$route['config']['PUT']                             = 'config/update_config';
$route['config/upload-logo']['POST']                = 'config/upload_logo';
$route['config/upload-attachment']['POST']          = 'config/upload_attachment';
$route['config/remove-attachment/(:any)']['DELETE'] = 'config/remove_attachment/$1';

#User
$route['user/login']['POST']                        = 'user/login';
$route['user/logout']['POST']                       = 'user/logout';
$route['user/refresh']['GET']                       = 'user/refresh';
$route['user/profile']['GET']                       = 'user/profile';
$route['user/registration']['POST']                 = 'user/registration';
$route['verification/(:any)']['GET']                = 'user/verification/$1';
$route['reset-password/(:any)']['GET']              = 'user/reset_password/$1';
$route['user/forget-password']['PUT']               = 'user/create_forget_password';
$route['user/set-new-password']['PUT']              = 'user/set_new_password';
$route['user/change-password']['PUT']               = 'user/change_password';
$route['user/firebase-token']['PUT']                = 'user/change_firebase_token';
$route['user']['PUT']                               = 'user/update_user';
$route['user']['GET']                               = 'user/get_user';
$route['user/by-id/(:any)']['GET']                  = 'user/get_user_by_id/$1';
$route['user/inactive/(:any)']['PUT']               = 'user/inactive/$1';
$route['user/active/(:any)']['PUT']                 = 'user/active/$1';
$route['user/count']['GET']                         = 'user/get_total_user';

#Report
$route['report']['POST']                      = 'report/create_report';
$route['report/upload-file']['POST']          = 'report/upload_file';
$route['report']['GET']                       = 'report/get_report';
$route['report/detail/(:any)']['GET']         = 'report/get_report_by_id/$1';
$route['report/count']['GET']                 = 'report/count_report';
$route['report/by-user']['GET']               = 'report/get_report_by_user';
$route['report/by-user/detail/(:any)']['GET'] = 'report/get_report_by_user_and_id/$1';
$route['report/by-user/count']['GET']         = 'report/count_report_by_user';
$route['report/respond']['PUT']               = 'report/respond_report';

#Notification
$route['notification/by-user']['GET']               = 'notification/get_notification_by_user';
$route['notification/by-user/count']['GET']         = 'notification/count_notification_by_user';
$route['notification/by-user/detail/(:any)']['GET'] = 'notification/get_notification_by_user_and_id/$1';
