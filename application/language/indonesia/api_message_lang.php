<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  #common idiom
  $lang['admin']          = 'admin';
  $lang['user']           = 'pengguna';
  $lang['config']         = 'pengaturan';
  $lang['report']         = 'pengaduan';
  $lang['attachment']     = 'file';
  $lang['authectication'] = 'otentikasi';
  $lang['verification']   = 'verifikasi';
  $lang['registration']   = 'registrasi';
  $lang['success']        = 'sukses';
  $lang['failed']         = 'gagal';
  $lang['not found']      = 'tidak ditemukan';
  $lang['not change']     = 'tidak berubah';
  #error message
  $lang['missing param']        = 'payload tidak memiliki parameter wajib';
  $lang['unauthorized access']  = 'akses tidak sah';
  $lang['invalid token']        = 'token tidak valid';
  $lang['internal server error']= 'terjadi kesalahan pada server';
  #action message
  $lang['create admin']                 = 'membuat admin';
  $lang['update admin']                 = 'mengubah admin';
  $lang['active admin']                 = 'mengaktifkan admin';
  $lang['inactive admin']               = 'menon-aktifkan admin';
  $lang['login']                        = 'login';
  $lang['logout']                       = 'logout';
  $lang['get profile']                  = 'mendapatkan profil';
  $lang['update profile']               = 'mengubah profil';
  $lang['get config']                   = 'mendapatkan pengaturan';
  $lang['update config']                = 'mengubah pengaturan';
  $lang['upload attachment']            = 'mengunggah file';
  $lang['remove attachment']            = 'menghapus file';
  $lang['create token']                 = 'membuat token';
  $lang['change password']              = 'mengubah kata sandi';
  $lang['change firebase token']        = 'mengubah token firebase';
  $lang['create report']                = 'membuat pengaduan';
  $lang['get report']                   = 'mendapatkan pengaduan';
  $lang['respond report']               = 'menanggapi pengaduan';
  $lang['get notification']             = 'mendapatkan notifikasi';
  $lang['get user']                     = 'mendapatkan pengguna';
  $lang['request forget password']      = 'permintaan lupa password';
  $lang['reset password']               = 'atur ulang kata sandi';
  $lang['change password']              = 'ganti kata sandi';

  $lang['email registered']          = 'email sudah terdaftar';
  $lang['invalid password']          = 'password tidak valid';
  $lang['old password invalid']      = 'password lama tidak valid';
  $lang['invalid verification']      = 'verifikasi tidak valid';
  $lang['report responded']          = 'pengaduan telah ditanggapi';
  $lang['invalid forget password']   = 'link reset kata sandi tidak valid';

?>