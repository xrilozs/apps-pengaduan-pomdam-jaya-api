<?php
  class Config_model extends CI_Model{
    public $id;
    public $twitter;
    public $instagram;
    public $youtube;
    public $whatsapp;
    public $description;
    public $logo_1;
    public $logo_2;

    function get_config(){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', logo_1) as full_logo_1,
        IFNULL(CONCAT('" . BASE_URL . "', logo_2), '') as full_logo_2
      ");
      $this->db->from('config');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function get_config_by_id($id){
      $this->db->where("id", $id);
      $this->db->from('config');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function create_config($data){
      $this->id         = $data['id'];
      $this->twitter    = $data['twitter'];
      $this->instagram  = $data['instagram'];
      $this->youtube    = $data['youtube'];
      $this->description= $data['description'];
      $this->logo_1     = $data['logo_1'];
      $this->logo_2     = array_key_exists("logo_2", $data) ? $data['logo_2'] : null;
      $this->whatsapp   = array_key_exists("whatsapp", $data) ? $data['whatsapp'] : null;
      $this->created_at = date("Y-m-d H:i:s");

      $this->db->insert('config', $this);
      return $this->db->affected_rows();
    }

    function update_config($data){
      $id = $data['id'];
      $this->db->update('config', $data, array("id"=>$id));
      return $this->db->affected_rows();
    }
    
  }
?>
