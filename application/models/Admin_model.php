<?php
  class Admin_model extends CI_Model{
    public $id;
    public $password;
    public $fullname;
    public $email;
    public $role;
    public $is_active;
    
    function get_admin_by_email($email, $is_active=null){
      $this->db->select("*");
      $this->db->where("email", $email);
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      $query = $this->db->get('admin');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_admin_by_id($id, $is_active=null){
      $this->db->select("*");
      $this->db->where("id", $id);
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      $this->db->from('admin');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function get_admins($search=null, $role=null, $order=null, $limit=null){
      $this->db->select("id, fullname, email, role, is_active, created_at, updated_at");
      if($search){
        $where_search = "(fullname LIKE '%$search%' OR email LIKE '%$search%')";
        $this->db->where($where_search);
      }
      if($role){
        $this->db->where('role', $role);
      }
      $this->db->from('admin');
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_admins($search=null, $role=null){
      $this->db->select("*");
      $this->db->from('admin');
      if($search){
        $where_search = "(fullname LIKE '%$search%' OR email LIKE '%$search%')";
        $this->db->where($where_search);
      }
      if($role){
        $this->db->where("role", $role);
      }
      return $this->db->count_all_results();
    }

    function create_admin($data){
      $this->id = $data['id'];
      $this->password = $data['password'];
      $this->fullname = $data['fullname'];
      $this->email = $data['email'];
      $this->role = $data['role'];
      $this->is_active = 1;
      $this->created_at = date("Y-m-d H:i:s");

      $this->db->insert('admin', $this);
      return $this->db->affected_rows() > 0;
    }

    function update_admin($data){
      $id = $data['id'];
      $this->id = $data['id'];
      $this->password = $data['password'];
      $this->fullname = $data['fullname'];
      $this->email = $data['email'];
      $this->role = $data['role'];
      $this->is_active = $data['is_active'];

      $this->db->update('admin', $this, array("id"=>$id));
      return $this->db->affected_rows();
    }

    function change_password($id, $password){
      $req = array(
        'password' => $password,
        'updated_at' => date("Y-m-d H:i:s")
      );
      $this->db->update('admin', $req, array('id'=>$id));
      return $this->db->affected_rows();
    }

    function active($id){
      $data = array(
        'is_active' => 1
      );
      $this->db->update('admin', $data, array('id'=>$id));
      return $this->db->affected_rows();
    }
    
    function inactive($id){
      $data = array(
        'is_active' => 0
      );
      $this->db->update('admin', $data, array('id'=>$id));
      return $this->db->affected_rows();
    }
  }
?>
