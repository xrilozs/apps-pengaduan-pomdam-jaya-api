<?php
  class Forget_password_model extends CI_Model{
    public $id;
    public $user_id;
    public $forget_password_code;
    public $is_used;

    function get_forget_password_by_code($code, $is_used=0){
      $this->db->where("forget_password_code", $code);
      $this->db->where("is_used", $is_used);
      $query = $this->db->get('forget_password');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_forget_password_by_id($id){
      $this->db->where("id", $id);
      $this->db->from('forget_password');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function create_forget_password($data){
      $this->id = $data['id'];
      $this->user_id = $data['user_id'];
      $this->forget_password_code = $data['forget_password_code'];
      $this->is_used = 0;
      $this->created_at = date("Y-m-d H:i:s");

      $this->db->insert('forget_password', $this);
      return $this->db->affected_rows() > 0;
    }

    function use_forget_password($id){
      $data = array(
        'is_used' => 1
      );
      $this->db->update('forget_password', $data, array('id'=>$id));
      return $this->db->affected_rows();
    }
  }
?>
