<?php
  class Materi_model extends CI_Model{
    public $id;
    public $rakernisId;
    public $title;
    public $description;
    public $materiUrl;
    public $type;
    public $createdAt;
    public $updatedAt;

    function get_materi($rakernis_id=null, $search=null, $type=null, $order=null, $limit=null){
      $this->db->select("*,
        (SELECT r.title FROM rakernis as r where r.id=rakernisId) as rakernisName,
        CONCAT('" . BASE_URL . "', materiUrl) as fullMateriUrl
      ");
      if($rakernis_id){
        $this->db->where("rakernisId", $rakernis_id);
      }
      if($search){
        $where_search = "(title LIKE '%$search%' OR description LIKE '%$search%')";
        $this->db->where($where_search);
      }
      if($type){
        $this->db->where("type", $type);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('materi');
      return $query->result();
    }

    function count_materi($rakernis_id=null, $search=null, $type=null){
      $this->db->select("*");
      $this->db->from('materi');
      if($rakernis_id){
        $this->db->where("rakernisId", $rakernis_id);
      }
      if($search){
        $where_search = "(title LIKE '%$search%' OR description LIKE '%$search%')";
        $this->db->where($where_search);
      }
      if($type){
        $this->db->where("type", $type);
      }
      return $this->db->count_all_results();
    }

    function get_materi_by_rakernis_id($rakernis_id, $order=null){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', materiUrl) as fullMateriUrl
      ");
      $this->db->where("rakernisId", $rakernis_id);
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      $query = $this->db->get('materi');
      return $query->result();
    }

    function get_materi_by_id($id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', materiUrl) as fullMateriUrl
      ");
      $this->db->where("id", $id);
      $query = $this->db->get('materi');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_materi($data){
      $this->id = $data['id'];
      $this->rakernisId = $data['rakernisId'];
      $this->title = $data['title'];
      $this->description = array_key_exists('description', $data) ? $data['description'] : null;
      $this->materiUrl = $data['materiUrl'];
      $this->type = $data['type'];
      $this->createdAt = date("Y-m-d H:i:s");

      $this->db->insert('materi', $this);
      return $this->db->affected_rows();
    }

    function update_materi($data){
      $id = $data['id'];
      $this->id = $data['id'];
      $this->rakernisId = $data['rakernisId'];
      $this->title = $data['title'];
      $this->description = array_key_exists('description', $data) ? $data['description'] : null;
      $this->materiUrl = $data['materiUrl'];
      $this->type = $data['type'];
      $this->createdAt = $data['createdAt'];
      $this->updatedAt = date("Y-m-d H:i:s");

      $this->db->update('materi', $this, array('id'=>$id));
      return $this->db->affected_rows();
    }

    function delete_materi($id){
      $this->db->where('id', $id);
      $this->db->delete('materi');
      return $this->db->affected_rows();
    }
  }
?>
