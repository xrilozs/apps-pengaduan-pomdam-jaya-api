<?php
  class Rakernis_model extends CI_Model{
    public $id;
    public $title;
    public $description;
    public $logoUrl;
    public $onboardUrl;
    public $backgroundUrl;
    public $layoutUrl;
    public $positionUrl;
    public $roadmapUrl;
    public $eventDate;
    public $location;
    public $locationUrl;
    public $status;
    public $createdAt;
    public $updatedAt;

    function get_rakernis($search=null, $status=null, $date=null, $order=null, $limit=null){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', logoUrl) as fullLogoUrl,
        CONCAT('" . BASE_URL . "', backgroundUrl) as fullBackgroundUrl,
        CONCAT('" . BASE_URL . "', onboardUrl) as fullOnboardUrl,
        CONCAT('" . BASE_URL . "', layoutUrl) as fullLayoutUrl,
        CONCAT('" . BASE_URL . "', positionUrl) as fullPositionUrl,
        CONCAT('" . BASE_URL . "', roadmapUrl) as fullRoadmapUrl,
        (SELECT AVG(r.rating) FROM reviews as r WHERE r.rakernisId=rakernis.id) as avgRating
      ");
      $this->db->where("status !=", 'DELETED');
      if($status && $status != 'DELETED'){
        $this->db->where("status", $status);
      }
      if($search){
        $where_search = "(title LIKE '%$search%' OR description LIKE '%$search%' OR location LIKE '%$search%')";
        $this->db->where($where_search);
      }
      if($date){
        $this->db->where("eventDate", $date);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('rakernis');
      return $query->result();
    }

    function get_rakernis_latest($status=null, $order=null){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', logoUrl) as fullLogoUrl,
        CONCAT('" . BASE_URL . "', onboardUrl) as fullOnboardUrl,
        CONCAT('" . BASE_URL . "', backgroundUrl) as fullBackgroundUrl,
        CONCAT('" . BASE_URL . "', layoutUrl) as fullLayoutUrl,
        CONCAT('" . BASE_URL . "', positionUrl) as fullPositionUrl,
        CONCAT('" . BASE_URL . "', roadmapUrl) as fullRoadmapUrl
      ");
      $this->db->where("status !=", 'DELETED');
      if($status && $status != 'DELETED'){
        $this->db->where("status", $status);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      $this->db->limit(1, 0);
      $query = $this->db->get('rakernis');
      return $query->result();
    }

    function get_rakernis_by_id($id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', logoUrl) as fullLogoUrl,
        CONCAT('" . BASE_URL . "', onboardUrl) as fullOnboardUrl,
        CONCAT('" . BASE_URL . "', backgroundUrl) as fullBackgroundUrl,
        CONCAT('" . BASE_URL . "', layoutUrl) as fullLayoutUrl,
        CONCAT('" . BASE_URL . "', positionUrl) as fullPositionUrl,
        CONCAT('" . BASE_URL . "', roadmapUrl) as fullRoadmapUrl
      ");
      $this->db->where("id", $id);
      $this->db->where("status !=", 'DELETED');
      $query = $this->db->get('rakernis');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function count_rakernis($search=null, $status=null, $date=null){
      $this->db->select("*");
      $this->db->from('rakernis');
      $this->db->where("status !=", 'DELETED');
      if($status && $status != 'DELETED'){
        $this->db->where("status", $status);
      }
      if($search){
        $where_search = "(title LIKE '%$search%' OR description LIKE '%$search%' OR location LIKE '%$search%')";
        $this->db->where($where_search);
      }
      if($date){
        $this->db->where("eventDate", $date);
      }
      return $this->db->count_all_results();
    }

    function create_rakernis($data){
      $this->id = $data['id'];
      $this->title = $data['title'];
      $this->description = $data['description'];
      $this->logoUrl = array_key_exists('logoUrl', $data) ? $data['logoUrl'] : null;
      $this->backgroundUrl = array_key_exists('backgroundUrl', $data) ? $data['backgroundUrl'] : null;
      $this->onboardUrl = array_key_exists('onboardUrl', $data) ? $data['onboardUrl'] : null;
      $this->layoutUrl = $data['layoutUrl'];
      $this->positionUrl = $data['positionUrl'];
      $this->roadmapUrl = $data['roadmapUrl'];
      $this->eventDate = $data['eventDate'];
      $this->location = $data['location'];
      $this->locationUrl = array_key_exists('locationUrl', $data) ? $data['locationUrl'] : null;
      $this->status = 'ON GOING';
      $this->createdAt = date("Y-m-d H:i:s");

      $this->db->insert('rakernis', $this);
      return $this->db->affected_rows();
    }

    function update_rakernis($data){
      $id = $data['id'];
      $this->id = $data['id'];
      $this->title = $data['title'];
      $this->description = $data['description'];
      $this->logoUrl = array_key_exists('logoUrl', $data) ? $data['logoUrl'] : null;
      $this->backgroundUrl = array_key_exists('backgroundUrl', $data) ? $data['backgroundUrl'] : null;
      $this->onboardUrl = array_key_exists('onboardUrl', $data) ? $data['onboardUrl'] : null;
      $this->layoutUrl = $data['layoutUrl'];
      $this->positionUrl = $data['positionUrl'];
      $this->roadmapUrl = $data['roadmapUrl'];
      $this->eventDate = $data['eventDate'];
      $this->location = $data['location'];
      $this->locationUrl = array_key_exists('locationUrl', $data) ? $data['locationUrl'] : null;
      $this->status = $data['status'];
      $this->createdAt = $data['createdAt'];
      $this->updatedAt = date("Y-m-d H:i:s");

      $this->db->update('rakernis', $this, array('id'=>$id));
      return $this->db->affected_rows();
    }

    function ongoing_rakernis($id){
      $data = array(
        "status" => 'ON GOING',
        "updatedAt" => date("Y-m-d H:i:s")
      );
      $this->db->where('id', $id);
      $this->db->update('rakernis', $data);
      return $this->db->affected_rows();
    }

    function done_rakernis($id){
      $data = array(
        "status" => 'DONE',
        "updatedAt" => date("Y-m-d H:i:s")
      );
      $this->db->where('id', $id);
      $this->db->update('rakernis', $data);
      return $this->db->affected_rows();
    }

    function soft_delete_rakernis($id){
      $data = array(
        "status" => 'DELETED',
        "updatedAt" => date("Y-m-d H:i:s")
      );
      $this->db->where('id', $id);
      $this->db->update('rakernis', $data);
      return $this->db->affected_rows();
    }
  }
?>
