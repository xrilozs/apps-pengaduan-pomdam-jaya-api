<?php
  class Reviews_model extends CI_Model{
    public $id;
    public $userId;
    public $rakernisId;
    public $rating;
    public $review;
    public $createdAt;
    public $updatedAt;

    function get_reviews($rakernis_id=null, $user_id=null, $search=null, $rating=null, $order=null, $limit=null){
      $this->db->select("*,
        (SELECT r.title FROM rakernis as r where r.id=rakernisId) as rakernisName,
        (SELECT u.fullname FROM users as u where u.id=userId) as userName
      ");
      if($rakernis_id){
        $this->db->where("rakernisId", $rakernis_id);
      }
      if($user_id){
        $this->db->where("userId", $user_id);
      }
      if($search){
        $this->db->like("review", $search);
      }
      if($rating){
        $this->db->where("rating", $rating);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('reviews');
      return $query->result();
    }

    function count_reviews($rakernis_id=null, $user_id=null, $search=null, $rating=null){
      $this->db->select("*");
      $this->db->from('reviews');
      if($rakernis_id){
        $this->db->where("rakernisId", $rakernis_id);
      }
      if($user_id){
        $this->db->where("userId", $user_id);
      }
      if($search){
        $this->db->like("review", $search);
      }
      if($rating){
        $this->db->where("rating", $rating);
      }
      return $this->db->count_all_results();
    }

    function get_review_by_rakernis_id_and_user_id($rakernis_id, $user_id){
      $this->db->select("*");
      $this->db->where("rakernisId", $rakernis_id);
      $this->db->where("userId", $user_id);
      $query = $this->db->get('reviews');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_review_by_id($id){
      $this->db->select("*");
      $this->db->where("id", $id);
      $query = $this->db->get('reviews');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_review($data){
      $this->id = $data['id'];
      $this->userId = $data['userId'];
      $this->rakernisId = $data['rakernisId'];
      $this->rating = $data['rating'];
      $this->review = $data['review'];
      $this->createdAt = date("Y-m-d H:i:s");

      $this->db->insert('reviews', $this);
      return $this->db->affected_rows();
    }

    function update_review($data){
      $id = $data['id'];
      $this->id = $data['id'];
      $this->userId = $data['userId'];
      $this->rakernisId = $data['rakernisId'];
      $this->rating = $data['rating'];
      $this->review = $data['review'];
      $this->createdAt = $data['createdAt'];
      $this->updatedAt = date("Y-m-d H:i:s");

      $this->db->update('reviews', $this, array('id'=>$id));
      return $this->db->affected_rows();
    }
  }
?>
