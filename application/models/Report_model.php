<?php
  class Report_model extends CI_Model{
    public $id;
    public $user_id;
    public $message;
    public $title;
    public $location;
    public $longitude;
    public $latitude;
    public $date;
    public $time;
    public $status;

    function get_report($user_id=null, $status=null, $order=null, $limit=null){
      $this->db->select("r.*,  u.fullname");
      if($status){
        $this->db->where("r.status", $status);
      }
      if($user_id){
        $this->db->where("r.user_id", $user_id);
      }
      if($order){
        $this->db->order_by('r.'.$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->join("user u", "u.id=r.user_id", "LEFT");
      $query = $this->db->get('report r');
      return $query->result();
    }

    function count_report($user_id=null, $status=null){
      $this->db->select("*");
      $this->db->from('report');
      if($status){
        $this->db->where("status", $status);
      }
      if($user_id){
        $this->db->where("user_id", $user_id);
      }
      return $this->db->count_all_results();
    }

    function get_report_by_id($id){
      $this->db->select("r.*,  u.fullname");
      $this->db->where("r.id", $id);
      $this->db->join("user u", "u.id = r.user_id", "LEFT");
      $query = $this->db->get('report r');
      return $query->num_rows() > 0 ? $query->row() : null;
    }
    
    function get_report_by_user_id_and_id($user_id, $id){
      $this->db->where("user_id", $user_id);
      $this->db->where("id", $id);
      $query = $this->db->get('report');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_report($data){
      $this->id         = $data['id'];
      $this->user_id    = $data['user_id'];
      $this->message    = $data['message'];
      $this->title      = $data['title'];
      $this->location   = $data['location'];
      $this->longitude  = $data['longitude'];
      $this->latitude   = $data['latitude'];
      $this->date       = $data['date'];
      $this->time       = $data['time'];
      $this->status     = 'SENT';
      $this->created_at  = date("Y-m-d H:i:s");

      $this->db->insert('report', $this);
      return $this->db->affected_rows();
    }

    function respond_report($id){
      $data = array(
        "status" => 'RESPONDED',
      );
      $this->db->where('id', $id);
      $this->db->update('report', $data);
      return $this->db->affected_rows();
    }
  }
?>
