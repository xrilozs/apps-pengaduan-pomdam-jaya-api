<?php
  class User_model extends CI_Model{
    public $id;
    public $password;
    public $email;
    public $handphone;
    public $fullname;
    public $status;

    function get_user_by_email($email, $is_active=0){
      $this->db->select("*");
      if($is_active){
        $this->db->where("status", 'ACTIVE');
      }
      $this->db->where("email", $email);
      $query = $this->db->get('user');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_user_by_id($id){
      $this->db->select("*");
      // $this->db->where("status !=", 'INACTIVE');
      $this->db->where("id", $id);
      $this->db->from('user');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function get_user($search=null, $status=null,  $order=null, $limit=null){
      $this->db->select("*");
      // $this->db->where('status !=', 'INACTIVE');
      if($status){
        $this->db->where('status', $status);
      }
      if($search){
        $where_search = "(fullname LIKE '%$search%' OR email LIKE '%$search%' OR handphone LIKE '%$search%')";
        $this->db->where($where_search);
      }
      $this->db->from('user');
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_user($search=null, $status=null){
      $this->db->select("*");
      $this->db->from('user');
      // $this->db->where("status !=", 'INACTIVE');
      if($search){
        $where_search = "(fullname LIKE '%$search%' OR email LIKE '%$search%' OR handphone LIKE '%$search%')";
        $this->db->where($where_search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      return $this->db->count_all_results();
    }

    function create_user($data){
      $this->id         = $data['id'];
      $this->password   = $data['password'];
      $this->email      = $data['email'];
      $this->handphone  = $data['handphone'];
      $this->fullname   = $data['fullname'];
      $this->status     = 'ONVERIFY';
      $this->created_at = date("Y-m-d H:i:s");

      $this->db->insert('user', $this);
      return $this->db->affected_rows() > 0;
    }

    function update_user($data){
      $id             = $data['id'];
      $this->id       = $data['id'];
      $this->password = $data['password'];
      $this->handphone= $data['handphone'];
      $this->email    = $data['email'];
      $this->fullname = $data['fullname'];
      $this->status   = $data['status'];

      $this->db->update('user', $this, array("id"=>$id));
      return $this->db->affected_rows();
    }

    function active_user($id){
      $data = array(
        'status' => 'ACTIVE'
      );
      $this->db->update('user', $data, array('id'=>$id));
      return $this->db->affected_rows();
    }

    function inactive_user($id){
      $data = array(
        'status' => 'INACTIVE'
      );
      $this->db->update('user', $data, array('id'=>$id));
      return $this->db->affected_rows();
    }

    function change_password_user($id, $password){
      $req = array(
        'password' => $password
      );
      $this->db->update('user', $req, array('id'=>$id));
      return $this->db->affected_rows();
    }

    function change_firebase_token_user($id, $firebase_token){
      $req = array(
        'firebase_token' => $firebase_token
      );
      $this->db->update('user', $req, array('id'=>$id));
      return $this->db->affected_rows();
    }
  }
?>
