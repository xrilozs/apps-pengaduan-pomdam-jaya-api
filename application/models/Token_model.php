<?php
  class Token_model extends CI_Model{
    public $id;
    public $user_id;
    public $token;
    public $is_expired;

    function get_token_by_token($token){
      $this->db->select("*");
      $this->db->where("is_expired", 0);
      $this->db->where("token", $token);
      $query = $this->db->get('token');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_token_by_id($id){
      $this->db->select("*");
      $this->db->where("id", $id);
      $query = $this->db->get('token');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_token($data){
      $this->id = $data['id'];
      $this->user_id = $data['user_id'];
      $this->token = $data['token'];
      $this->is_expired = 0;
      $this->created_at = date("Y-m-d H:i:s");

      $this->db->insert('token', $this);
      return $this->db->affected_rows();
    }

    function expire_token($id){
      $data = array(
        'is_expired' => 1
      );
      $this->db->update('token', $data, array('id'=>$id));
      return $this->db->affected_rows();
    }
  }
?>
