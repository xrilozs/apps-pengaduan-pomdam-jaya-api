<?php
  class Notification_model extends CI_Model{
    public $id;
    public $user_id;
    public $report_id;
    public $title;
    public $message;
    public $is_read;
    public $date;
    public $time;

    function get_notification($user_id=null, $is_read=null, $order=null, $limit=null){
      $this->db->select("n.*,  u.fullname");
      if(!is_null($is_read)){
        $this->db->where("n.is_read", $is_read);
      }
      if($user_id){
        $this->db->where("n.user_id", $user_id);
      }
      if($order){
        $this->db->order_by('n.'.$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->join("user u", "u.id=n.user_id", "LEFT");
      $query = $this->db->get('notification n');
      return $query->result();
    }

    function count_notification($user_id=null, $is_read=null){
      $this->db->from('notification');
      if(!is_null($is_read)){
        $this->db->where("is_read", $is_read);
      }
      if($user_id){
        $this->db->where("user_id", $user_id);
      }
      return $this->db->count_all_results();
    }

    function get_notification_by_id($id){
      $this->db->select("n.*,  u.fullname");
      $this->db->where("n.id", $id);
      $this->db->join("user u", "u.id = n.user_id", "LEFT");
      $query = $this->db->get('notification n');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_notification_by_report_id($report_id){
      $this->db->select("n.*,  u.fullname");
      $this->db->where("n.report_id", $report_id);
      $this->db->join("user u", "u.id = n.user_id", "LEFT");
      $query = $this->db->get('notification n');
      return $query->num_rows() > 0 ? $query->row() : null;
    }
    
    function get_notification_by_user_id_and_id($user_id, $id){
      $this->db->where("user_id", $user_id);
      $this->db->where("id", $id);
      $query = $this->db->get('notification');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_notification($data){
      $this->id         = $data['id'];
      $this->user_id    = $data['user_id'];
      $this->report_id  = $data['report_id'];
      $this->title      = $data['title'];
      $this->message    = $data['message'];
      $this->is_read    = 0;
      $this->date       = date('Y-m-d');
      $this->time       = date('H:i');
      $this->created_at  = date("Y-m-d H:i:s");

      $this->db->insert('notification', $this);
      return $this->db->affected_rows();
    }

    function read_notification($id){
      $data = array(
        "is_read" => 1,
      );
      $this->db->where('id', $id);
      $this->db->update('notification', $data);
      return $this->db->affected_rows();
    }
  }
?>
