<?php
  class Verification_model extends CI_Model{
    public $id;
    public $user_id;
    public $verification_code;
    public $is_used;

    function get_verification_by_code($code){
      $this->db->where("verification_code", $code);
      $query = $this->db->get('verification');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_verification_by_id($id){
      $this->db->select("*");
      $this->db->where("id", $id);
      $this->db->from('verification');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function create_verification($data){
      $this->id = $data['id'];
      $this->user_id = $data['user_id'];
      $this->verification_code = $data['verification_code'];
      $this->is_used = 0;
      $this->created_at = date("Y-m-d H:i:s");

      $this->db->insert('verification', $this);
      return $this->db->affected_rows() > 0;
    }

    function use_verification($id){
      $data = array(
        'is_used' => 1
      );
      $this->db->update('verification', $data, array('id'=>$id));
      return $this->db->affected_rows();
    }
  }
?>
