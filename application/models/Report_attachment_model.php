<?php
  class Report_attachment_model extends CI_Model{
    public $id;
    public $report_id;
    public $type;
    public $url;

    function get_report_attachment(){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', url) as full_url
      ");
      $this->db->from('report_attachment');
      $query = $this->db->get();
      return $query->result();
    }

    function get_report_attachment_by_id($id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', url) as full_url
      ");
      $this->db->where("id", $id);
      $this->db->from('report_attachment');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function get_report_attachment_by_report_id($report_id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', url) as full_url
      ");
      $this->db->where("report_id", $report_id);
      $this->db->from('report_attachment');
      $query = $this->db->get();
      return $query->result();
    }

    function create_report_attachment($data){
      $this->id         = $data['id'];
      $this->report_id  = $data['report_id'];
      $this->type       = $data['type'];
      $this->url        = $data['url'];
      $this->created_at = date("Y-m-d H:i:s");

      $this->db->insert('report_attachment', $this);
      return $this->db->affected_rows();
    }    
  }
?>
