<?php
  class Config_attachment_model extends CI_Model{
    public $id;
    public $type;
    public $url;

    function get_config_attachment(){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', url) as full_url
      ");
      $this->db->from('config_attachment');
      $query = $this->db->get();
      return $query->result();
    }

    function get_config_attachment_by_id($id){
      $this->db->select("*,
        CONCAT('" . BASE_URL . "', url) as full_url
      ");
      $this->db->where("id", $id);
      $this->db->from('config_attachment');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function create_config_attachment($data){
      $this->id         = $data['id'];
      $this->type       = $data['type'];
      $this->url        = $data['url'];
      $this->created_at = date("Y-m-d H:i:s");

      $this->db->insert('config_attachment', $this);
      return $this->db->affected_rows();
    }

    function remove_config_attachment($id){
      $this->db->where('id', $id);
      $this->db->delete('config_attachment');
      return $this->db->affected_rows();
    }
    
  }
?>
